from helper_functions import read_input


shape_notation = {
    'A': 'Rock',
    'X': 'Rock',
    'B': 'Paper',
    'Y': 'Paper',
    'C': 'Scissors',
    'Z': 'Scissors'
}

# Key wins over value
wins = {
    'Rock': 'Scissors',
    'Scissors': 'Paper',
    'Paper': 'Rock'
}

# Key gets countered by value
counters = {
    'Rock': 'Paper',
    'Paper': 'Scissors',
    'Scissors': 'Rock'
}

shape_score = {
    'Rock': 1,
    'Paper': 2,
    'Scissors': 3
}


def check_fight_outcome(player_shape: str, opponent_shape: str) -> int:
    outcome = shape_score[player_shape]

    if player_shape == opponent_shape:
        outcome += 3

    elif wins[player_shape] == opponent_shape:
        outcome += 6

    return outcome


def choose_shape(opponent_shape: str, desired_outcome: str) -> str:
    # Lose
    if desired_outcome == 'X':
        return wins[opponent_shape]

    # Draw
    elif desired_outcome == 'Y':
        return opponent_shape

    # Win
    elif desired_outcome == 'Z':
        return counters[opponent_shape]

    raise ValueError


def part_one(input_arr: list[str]) -> int:
    total = 0

    for line in input_arr:
        split = line.split(' ')

        opponent_shape = shape_notation[split[0]]
        player_shape = shape_notation[split[1]]

        total += check_fight_outcome(player_shape, opponent_shape)

    return total


def part_two(input_arr: list[str]) -> int:
    total = 0

    for line in input_arr:
        split = line.split(' ')
        opponent_shape = shape_notation[split[0]]
        desired_outcome = split[1]

        player_shape = choose_shape(opponent_shape, desired_outcome)
        total += check_fight_outcome(player_shape, opponent_shape)

    return total


if __name__ == "__main__":
    input_arr = read_input("day-02.txt")
    print(f'Part one: {part_one(input_arr)}')
    print(f'Part two: {part_two(input_arr)}')
