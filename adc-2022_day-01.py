from helper_functions import read_input


def part_one(input_arr: list[str]) -> int:
    max_calories = 0
    current_total_calories = 0

    for line in input_arr:
        if line:
            current_total_calories += int(line, 10)

        else:
            max_calories = max(max_calories, current_total_calories)
            current_total_calories = 0

    return max_calories


def part_two(input_arr: list[str]) -> int:
    total_calories_arr = []
    current_total_calories = 0

    for line in input_arr:
        if line:
            current_total_calories += int(line, 10)

        else:
            total_calories_arr.append(current_total_calories)
            current_total_calories = 0

    total_calories_arr.sort(reverse=True)
    return sum(total_calories_arr[:3])


if __name__ == "__main__":
    input_arr = read_input("day-01.txt")
    print(f'Part one: {part_one(input_arr)}')
    print(f'Part two: {part_two(input_arr)}')
