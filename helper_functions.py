def read_input(filename: str) -> list[str]:
    input_arr = []

    with open(f'inputs/{filename}', "r") as f:
        input_arr = f.read().splitlines()

    return input_arr


def split_list_into_chunks(l: list, chunk_size: int):
    return [l[i:i+chunk_size] for i in range(0, len(l), chunk_size)]


def is_between(x: int, a: int, b: int, strict: bool = False) -> bool:
    min_val = min(a, b)
    max_val = b if min_val == a else b

    if strict:
        return min_val < x and x < max_val

    return min_val <= x and x <= max_val
