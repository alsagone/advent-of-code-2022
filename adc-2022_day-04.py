from helper_functions import read_input, is_between
import re
import sys


class Range:
    def __init__(self, start: int, end: int) -> None:
        self.start = start
        self.end = end

    def __str__(self) -> str:
        return f"({self.start} - {self.end})"


def overlaps_completely(r1: Range, r2: Range) -> bool:
    if r1.start == r2.start:
        return True

    #(2-8, 3-7)
    if r1.start < r2.start:
        return is_between(r2.start, r1.start, r1.end) and is_between(r2.end, r1.start, r1.end)

    #(6,6, 4-6)
    return is_between(r1.start, r2.start, r2.end) and is_between(r1.end, r2.start, r2.end)


def overlaps(r1: Range, r2: Range) -> bool:
    if r1.start == r2.start or r1.end == r2.end:
        return True

    if r1.start < r2.start:
        return is_between(r2.start, r1.start, r1.end) or is_between(r2.end, r1.start, r1.end)

    return is_between(r1.start, r2.start, r2.end) or is_between(r1.end, r2.start, r2.end)


def part_one(input_arr: list[str]):
    pattern = re.compile(r"(\d+)")
    total_overlaps = 0

    for line in input_arr:
        match = re.findall(pattern, line)

        if len(match) != 4:
            print(f"Match error on string '{line}'")
            sys.exit(1)

        r1 = Range(int(match[0]), int(match[1]))
        r2 = Range(int(match[2]), int(match[3]))

        if overlaps_completely(r1, r2):
            total_overlaps += 1

    return total_overlaps


def part_two(input_arr: list[str]):
    pattern = re.compile(r"(\d+)")
    total_overlaps = 0

    for line in input_arr:
        match = re.findall(pattern, line)

        if len(match) != 4:
            print(f"Match error on string '{line}'")
            sys.exit(1)

        r1 = Range(int(match[0]), int(match[1]))
        r2 = Range(int(match[2]), int(match[3]))

        if overlaps(r1, r2):
            total_overlaps += 1

    return total_overlaps


if __name__ == "__main__":
    input_arr = read_input("day-04.txt")
    print(f'Part one: {part_one(input_arr)}')
    print(f'Part two: {part_two(input_arr)}')
