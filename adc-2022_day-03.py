from helper_functions import read_input, split_list_into_chunks


def split_string_half(string: str):
    half_point = len(string) // 2
    left_part = string[:half_point]

    if len(string) % 2 == 0:
        right_part = string[half_point:]

    else:
        right_part = string[half_point+1:]

    return left_part, right_part


def find_common_char(list_strings: list[str]) -> str:
    intersection = set.intersection(*map(set, list_strings))
    return intersection.pop()


def get_char_priority(char: str) -> int:
    offset = 96 if char.islower() else 38
    return ord(char) - offset


def part_one(input_arr: list[str]) -> int:
    total = 0

    for line in input_arr:
        left_part, right_part = split_string_half(line)
        common_char = find_common_char([left_part, right_part])
        total += get_char_priority(common_char)

    return total


def part_two(input_arr: list[str]) -> int:
    total = 0

    for group in split_list_into_chunks(input_arr, 3):
        common_char = find_common_char(group)
        total += get_char_priority(common_char)

    return total


if __name__ == "__main__":
    input_arr = read_input("day-03.txt")
    print(f'Part one: {part_one(input_arr)}')
    print(f'Part two: {part_two(input_arr)}')
