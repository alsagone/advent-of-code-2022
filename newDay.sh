#!/bin/bash 

function touch_file {
    for filename in $@
    do
        touch "$filename"

        if [ $? -eq 0 ]; then
            echo "${filename} created"

        else
            echo "Unable to create ${filename}"
            exit 1
        fi
    done
}

nb_adc_scripts=$(find . -maxdepth 1 -type f -name "adc*" -printf x | wc -c)
((nb_adc_scripts+=1))

# Pad the number to two digits 
printf -v x "%02d" $nb_adc_scripts

touch_file "adc-2022_day-$x.py" "inputs/day-$x.txt"